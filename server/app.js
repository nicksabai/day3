console.log("Starting...");
var express = require("express");
var app = express ();

const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client"));

//app.use('/bower_components', express.static(__dirname + "/../client/bower_components"));

app.listen(NODE_PORT, function(){
    console.log("Web App started at " + NODE_PORT);
});

app.use(function(res, req){
    console.log("Sorry, the page you requested is not found.");
    res.send("Sorry, the page you requested is not found.");
});