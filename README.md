## Prepare Project
1. mkdir <project directory>
1. mkdir <project directory>/server
1. mkdir <project directory>/client
1. cd server
1. touch app.js

## Global Utility Installation <project directory>
1. sudo npm install nodemon -g
1. sudo npm install bower -g

## Express Installation <project directory> (Local Utility)
1. sudo npm install express -g

## Create package.json File <project directory>
1. npm init

## Bitbucket
1. Create new repo on Bitbucket
1. Get info, eg.(git remote add origin https://nicksabai@bitbucket.org/nicksabai/day3.git)

## Git Initialize
1. git init in <project directory>
1. git remote add origin https://nicksabai@bitbucket.org/nicksabai/day3.git
1. git add .
1. git commit -m "Remarks go here"
1. git push origin master

## Set Port for App
1. export NODE_PORT = 3000

## Set Up Bower
1. Create .bowerrc in <project directory>
1. In .bowerrc {"directory": "client/bower_components"}
1. bower init in <project directory> (initialize bower.json file)

## Install Angular
1. bower install angular --save

## Install Bootstrap
1. bower install bootstrap --save

## Install Font Awesome
1. bower install font-awesome --save

## Watch Node
1. nodemon

## Launch App
1. In browser url bar, type in localhost:3000

## Misc commands
1. git remote -v (checks whether fetch and push is correct)
1. git checkout -b feature_1 (creates a branch)
1. git checkout master (switches out from branch to master)
1. nodemon --version (checks version)
1. rs (restarts nodemon in case of error)



